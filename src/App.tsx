import React from 'react';
import './App.css';
import WorkoutsPage from "./Pages/WorkoutsPage/workoutsPage";

function App() {
    return (
        <WorkoutsPage/>
    );
}

export default App;
