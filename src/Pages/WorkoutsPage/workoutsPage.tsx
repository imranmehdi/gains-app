import {WorkoutList} from "./Components/workoutList";
import {Page} from "../../Components/page";
import {EmptyState} from "../../Components/EmptyState/emptyState";
import {IconButton} from "../../Components/iconButton";

const WorkoutsPage = () => {
    const workouts = [];


    return (<Page title="Workouts">
        {(workouts.length === 0) && <EmptyState type="workouts"/>}

        <WorkoutList/>

        <div style={{display: "flex", alignItems: "flex-end", justifyContent: "flex-end"}}>
            <IconButton handleClick={() => console.log("")}></IconButton>
        </div>
    </Page>)
}

export default WorkoutsPage
