import {WorkoutItem} from "./workoutItem";

export const WorkoutList = () => {
    return (
        <>
            {["test"].map((title) => <WorkoutItem title={title}/>)}
        </>
    )
}
