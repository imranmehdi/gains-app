interface WorkoutItemProps {
title: string;
}

export const WorkoutItem = ({ title }: WorkoutItemProps) => {
    return <div>
        <p>{title}</p>
    </div>
};
