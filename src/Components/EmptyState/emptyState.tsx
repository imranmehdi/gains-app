import "./emptyState.css";

interface EmptyStateProps {
    type: string;
}

export const EmptyState = ({type}: EmptyStateProps) => {
    return (
        <div className="container">
            <img src={"assets/icon/noDataIcon.png"} alt="" width="450px"/>

            <strong>No {type} yet!</strong>

            <p>Create one by tapping the floating button</p>
        </div>
    );
};
