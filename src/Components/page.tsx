import {ReactNode} from "react";
import {Navbar} from "./navbar";

interface PageProps {
    children: ReactNode
    title: string
}

export const Page = ({children, title}: PageProps) => {
    return <div style={{height: "100vh"}}>
        <Navbar title={title}/>

        {children}
    </div>
}
