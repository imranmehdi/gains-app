interface NavbarProps {
    title?: string;
}

export const Navbar = ({title}: NavbarProps) => {
    return (
        <div style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            width: "100%"
        }}>
            {title && (
                <h1
                    style={{
                        fontFamily: "Roboto",
                        fontStyle: "normal",
                        fontWeight: "700",
                        fontSize: " 18px",
                        lineHeight: "28px",
                        color: "#474141",
                    }}
                >
                    {title}
                </h1>
            )}
        </div>
    );
};
