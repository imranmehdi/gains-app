interface IconButtonProps {
    handleClick: () => void
    iconSrc?: string
}

export const IconButton = ({handleClick, iconSrc}: IconButtonProps) => {
    return <button className="icon-button"
                   onClick={handleClick}
                   style={{
                       display: "flex",
                       alignItems: "center",
                       justifyContent: "center",
                       height: "45px",
                       width: "45px",
                       borderRadius: "12px",
                   }}
                   color="light"
    >
        <img src={iconSrc} alt="" width="14px"/>
    </button>
}
